import {useContext, useEffect} from "react";
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";

export default function Logout(){
	// localStorage.clear();

	//Consume the UserContext object and destructure it to access the
	const {setUser, unsetUser} = useContext(UserContext);

	//Clear the local storage of the user's information
	unsetUser();
	// console.log(user);

	useEffect(() =>{
		setUser({

			id: null,
			isAdmin: null
		})
	})

	return(

		//redirect back to login
		<Navigate to="/login" />

	)
}