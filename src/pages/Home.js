import Banner from "../components/Banner"
import Higlights from "../components/Higlights"

export default function Home() {
	const data = {
		title: "Zuit Coding Bootcamp",
		content: "Oppurtunities for everyone, everywhere.",
		destination: "/courses",
		label: "Enroll"
	}

	return(
		<>
			<Banner data={data} />
        	<Higlights/>

		</>

	)

}